<?php

if ($data != null) {
    $page = <<<HTML
	<div class="confirm">
		<form action="/blog/register" method="post">
			<p>{$data}</p>
			<input type="submit" name="home" value="Back to registration">
		</form>
	</div>
HTML;
} else {
    $page = <<<HTML
<div class="reg form">
    <form name="reg_form" method="post" action="/blog/register" onsubmit="return validate_form();">
        <label for="log">Login</label><input id ="log" type="text" name="login"><!--
        --><label for="passwd">Password</label><!--
        --><input id ="passwd" type="password" name="password"><!--
        --><label for="passwd_2">Confirm password</label><!--
        --><input id ="passwd_2" type="password" name="password_2"><!--
        --><label for="email">Email</label><input id ="email" type="email" name="email"><br>
        <input type="submit" name="register" value="OK">
    </form>
	<p id="message"></p>
</div>
HTML;
}
echo $page;
?>

<script type="text/javascript">
	function validate_form()
	{
	    var valid = false;
	    var msg = '';
	    if (document.reg_form.login.value === "")
        	msg = "Please enter your login";
	    else if (document.reg_form.password.value === "")
        	msg = "Please enter your password";
	    else if (document.reg_form.password_2.value === "")
	        	msg = "Please confirm your password";
	    else if (document.reg_form.email.value === "")
	        	msg = "Please enter your email";
	    else if (document.reg_form.passwd.value !== document.reg_form.passwd_2.value)
	        	msg = "Please enter the same password twice";
	    else
	        valid = true;
	    write_message(msg);
	    return valid;
	}

	function write_message(msg)
	{
	    var par = document.getElementById('message');
	    par.innerHTML = msg;
	}
</script>
