<?php $data['article_text'] = htmlspecialchars($data['article_text']);?>

<div class="one-article">
        <?php if (($user['is_author'])) {
            $content = <<<HTML
    <form method="post">
    <textarea autofocus name="text">{$data['article_text']}</textarea>
        <span>{$data['user_login']}</span>
        <input type="hidden" name="article_id" value="{$data['article_id']}">
        <input type="submit" name="delete_blog" value="delete">
        <input type="submit" name="update_blog" value="update">
    </form>
HTML;
    } else {
        $content = <<<HTML
    <p name="text">{$data['article_text']}</p>
        <span>{$data['user_login']}</span>

HTML;
    }
    echo $content;
    ?>
</div>
