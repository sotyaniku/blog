<div class="reg form">
    <form name="log_form" method="post" action="/blog/login" onsubmit="return validate_form();">
        <label for="log">Login</label><input id ="log" type="text" name="login"><br><!--
        --><label for="passwd">Password</label><input id ="passwd" type="password" name="password">
        <input type="submit" name="log-in" value="OK">
    </form>
    <p id="message"><?php echo $data;?></p>
</div>
<script type="text/javascript">
	function validate_form()
	{
	    var valid = true;
	    if (document.log_form.login.value == "" || document.log_form.password.value == "") {
            valid = false;
            var msg = "please fill all the fields";
            write_message(msg);
        }
       	return valid;
	}

    function write_message(msg)
    {
        var par = document.getElementById('message');
        par.innerHTML = msg;
    }

</script>
