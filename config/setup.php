#!/usr/bin/php
<?php
$paramsPath = 'database.php';
include($paramsPath);

try {
	$db = new PDO(DB_DSN, DB_USER, DB_PASSWORD);
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
	echo 'Connection failed: ' . $e->getMessage();
}
$sql = $db->prepare("CREATE DATABASE IF NOT EXISTS blog");
$sql->execute();
$sql = $db->prepare(file_get_contents("blog.sql"));
$sql->execute();
?>