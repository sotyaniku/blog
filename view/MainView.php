<div class="articles">
    <?php foreach ($data as $articleItem) :?>
    <div>
        <a href="article/<?php echo ($articleItem['article_id'])?>"><?php echo htmlspecialchars($articleItem['article_text'])?></a>
        <span><?php echo htmlspecialchars($articleItem['user_login'])?></span>
        <?php if (!empty($user['author']) && $user['author'] === $articleItem['user_login']) {
            $buttons = <<<HTML
<form class="buttons" method="post">
        <input type="hidden" name="article_id" value="{$articleItem['article_id']}">
        <input type="submit" name="delete_blog" value="delete">
        <input type="submit" name="update_blog" value="update">
    </form>
HTML;
            echo $buttons;
        }?>
    </div>
    <?php endforeach;?>
</div>
<?php if (!empty($user['author'])) {
    $form = <<<HTML
<div class="add-article">
    <form method="post">
        <h3>Write your text here</h3>
        <textarea name="text"></textarea><br>
        <input type="submit" name="add_blog" value="save">
    </form>
</div>

HTML;
} else {
    $form = '';
}
echo $form;
?>
