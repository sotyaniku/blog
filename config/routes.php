<?php

const ROUTES = array (
    'article/([a-z0-9]+)' => 'article/view/$1',
    'article' => 'article/index',
    'main' => 'user/index',
    'logout' => 'user/logout',
    'login' => 'user/login',
    'register' => 'user/register',
    'blog' => 'blog/article/index'
);
