<?php

require_once ROOT . '/model/Db.php';

class User {

    public static function getUserByEmail($email) {
        $db = Db::getConnection();

        $sql = "SELECT * FROM tbl_user WHERE user_mail = :email";
        $sth = $db->prepare($sql);
        $sth->execute(array(
            "email" => $email
        ));
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $user = $sth->fetch();

        return $user;
    }

    public static function getUserById($id) {
        $db = Db::getConnection();

        $sql = "SELECT * FROM tbl_user WHERE user_id = :id";
        $sth = $db->prepare($sql);
        $sth->execute(array(
            "id" => $id
        ));
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $user = $sth->fetch();

        return $user;
    }

    public static function getUserByLogin($login) {
        $db = Db::getConnection();

        $sql = "SELECT * FROM tbl_user WHERE user_login = :login";

        $sth = $db->prepare($sql);
        $sth->execute(array(
            "login" => $login
        ));
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $user = $sth->fetch();

        return $user;
    }

    public static function getUserArticlesByLogin($login) {
        $db = Db::getConnection();

        $sql = "SELECT tbl_user.*, tbl_article.article_id, 
                tbl_article.article_text FROM tbl_user " .
               "INNER JOIN tbl_article " .
               "ON tbl_user.user_id = tbl_article.user_id " .
               "WHERE tbl_user.user_login = :login";

        $sth = $db->prepare($sql);
        $sth->execute(array(
            "login" => $login
        ));
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $i = 0;
        $user = array();
        while ($row = $sth->fetch())
            $user[$i++] = $row;
        return $user;
    }

    public static function addUserToDb($login, $email, $password) {
        $db = Db::getConnection();

        $sql = "INSERT INTO tbl_user " .
               "(user_login, user_email, user_password) " .
               "VALUES (:login, :email, :password)";
        $sth = $db->prepare($sql);
        $sth->execute(array(
            "login" => $login,
            "password" => $password,
            "email" => $email,
        ));
    }

}