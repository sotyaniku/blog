<?php

require_once ROOT . '/model/Db.php';

class Article {

    public static function getArticleById($id) {
        $db = Db::getConnection();

        $sql = "SELECT tbl_article.*, tbl_user.user_login FROM tbl_article " .
               "INNER JOIN tbl_user " .
               "ON tbl_article.user_id = tbl_user.user_id " .
               "WHERE article_id = :id";

        $sth = $db->prepare($sql);
        $sth->execute(array(
            "id" => $id
        ));
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $article = $sth->fetch();

        return $article;
    }

    public static function addArticle($user_id, $text) {
        $db = Db::getConnection();

        $sql = "INSERT INTO tbl_article (user_id, article_text) VALUES (:id, :text)";

        $sth = $db->prepare($sql);
        $sth->execute(array(
            "id" => $user_id,
            "text" => $text
        ));
    }

    public static function updateArticle($id, $text) {
        $db = Db::getConnection();

        $sql = "UPDATE tbl_article SET article_text = :text " .
               "WHERE article_id = :id";

        $sth = $db->prepare($sql);
        $sth->execute(array(
            "id" => $id,
            "text" => $text
        ));
    }

    public static function deleteArticle($id) {
        $db = Db::getConnection();

        $sql = "DELETE FROM tbl_article WHERE article_id = :id";

        $sth = $db->prepare($sql);
        $sth->execute(array(
            "id" => $id,
        ));
    }

    public static function getArticles() {
        $db = Db::getConnection();

        $List = array();
        $sql = "SELECT tbl_article.*, tbl_user.user_login FROM tbl_article " .
               "INNER JOIN tbl_user " .
               "ON tbl_article.user_id = tbl_user.user_id";
        $result = $db->query($sql);
        $i = 0;
        while ($row = $result->fetch()) {
            $List[$i++] = $row;
        }

        return $List;
    }

}