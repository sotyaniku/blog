<?php

const ROOT = __DIR__;
require_once ROOT . '/config/site.php';
require_once ROOT . '/config/database.php';
require_once ROOT . '/config/routes.php';

require_once 'config/model.php';
require_once 'config/view.php';
require_once 'config/controller.php';

ini_set('display_errors', 1);
error_reporting(E_ALL);
session_start();

set_error_handler(function ($severity, $message, $file, $line) {
    if (!(error_reporting() & $severity)) return;

    throw new ErrorException($message, 0, $severity, $file, $line);
});


require_once ROOT . '/components/Router.php';

$router = new Router(ROUTES);
$router->run();
