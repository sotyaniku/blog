<!DOCTYPE html>
<html lang="en">
<html>
	<head>
		<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" type="text/css" href="/blog/webroot/css/style.css">
        </head>
	<body>
        <div>
            <menu>
                <div>
                    <ul class="menu">
                        <li><a href="/blog/article">Blog</a></li><!--
                    --><li><a href="/blog/login">Log in</a></li><!--
                    --><li><a href="/blog/register">Register</a></li>
                    </ul>
                </div>
            </menu>
    	    <?php include 'view/' . $content_view; ?>
        </div>
    </body>
</html>
