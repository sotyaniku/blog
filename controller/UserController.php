<?php

require_once ROOT . '/model/User.php';
require_once ROOT . '/model/Article.php';

class UserController extends Controller {

    private $user;
    private $username;
    private $password;
    private $email;

    public function __construct() {
        parent::__construct();
        if (!empty($_SESSION['user'])) {
            $this->user = User::getUserByLogin($_SESSION['user']);
        }
    }

    public function actionIndex() {

        if ($this->user) {
            $this->view->generate('MainView.php', 'TemplateView.php', $this->user);

            return;
        }
    }

    public function actionLogout() {
        $_SESSION['logged_user'] = '';
        session_destroy();
        header('Location: /blog/');
    }

    public function actionRegister() {
        $error_msg = '';

        if (!empty($_POST['register'])) {
            if ($this->_findUserInDb($_POST['login'])) {
                $error_msg = "A user with such login already exists";
            } else if ($this->_checkEmailInDb($_POST['email'])) {
                $error_msg = "A user with such email already exists";
            } else {
                $this->username = $_POST['login'];
                $passwordHash = hash('whirlpool', $_POST['password']);
                $this->password = $passwordHash;
                $this->email = $_POST['email'];

                User::addUserToDb($this->username, $this->email, $this->password);
                header('Location: /blog/login');
            }
        }
        $this->view->generate('RegisterView.php', 'TemplateView.php', $error_msg);
    }

    public function actionLogin() {
        $error_msg = '';
        if (!empty($_POST['log-in'])) {
            $passwordHash = hash('whirlpool', $_POST['password']);
            if (!$this->_checkUserAuth($_POST['login'], $passwordHash)) {
                $error_msg = 'Invalid login/password combination';
            } else {
                $_SESSION['user'] = $_POST['login'];
                header('Location: /blog');
            }
        }
        $this->view->generate('LoginView.php', 'TemplateView.php', $error_msg);
    }

    private function _findUserInDb($login)
    {
        $user = User::getUserByLogin($login);
        return $user;
    }

    private function _checkEmailInDb($email)
    {
        $user = User::getUserByEmail($email);
        return $user;
    }

    private function _checkUserAuth($login, $password)
    {
        if (!($user = User::getUserByLogin($login)))
            return 0;
        return ($user['user_login'] == $login && $user['user_password'] == $password);
    }

}


