<?php

require_once ROOT . '/model/Article.php';
require_once ROOT . '/model/User.php';

class ArticleController extends Controller {

    public function actionIndex() {
        if (!empty($_POST['add_blog']) && !empty($_SESSION['user'])) {
            $user = User::getUserByLogin($_SESSION['user']);
            if ($user)
                Article::addArticle($user['user_id'], $_POST['text']);
        }
        else if (!empty($_POST['delete_blog'])) {
            Article::deleteArticle($_POST['article_id']);
        }
        else if (!empty($_POST['update_blog'])) {
            header('Location: article/' . $_POST['article_id']);
        }

        $articleList = Article::getArticles();
        $user['author'] = (!empty($_SESSION['user'])) ? $_SESSION['user'] : '';
        $template = !empty($_SESSION['user']) ? 'ActiveTemplateView.php' : 'TemplateView.php';
        $this->view->generate('MainView.php', $template, $articleList, $user);

        return true;
    }

    public function actionView($id) {

        if (!empty($_POST['delete_blog'])) {
            Article::deleteArticle($_POST['article_id']);
            header('Location: /blog/');
        }

        else if (!empty($_POST['update_blog']) && !empty ($_POST['text'])) {
            Article::updateArticle($id, $_POST['text']);
            header('Location: /blog/');
        }

        $article = Article::getArticleById($id);
        $user['is_author'] = (!empty($article['user_login']) && !empty($_SESSION['user']) && $article['user_login'] === $_SESSION['user']) ? true : false;

        $template = !empty($_SESSION['user']) ? 'ActiveTemplateView.php' : 'TemplateView.php';
        $this->view->generate('ArticleView.php', $template, $article, $user);
    }
}
