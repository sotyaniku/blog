CREATE TABLE tbl_article1
(
	article_id int auto_increment
		primary key,
	user_id int null,
	article_text text null
)
;

CREATE TABLE tbl_user1
(
	user_id int auto_increment
		primary key,
	user_login varchar(255) null,
	user_password varchar(255) null,
	user_email varchar(255) null
)
;